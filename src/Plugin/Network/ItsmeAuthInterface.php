<?php

namespace Drupal\social_auth_itsme\Plugin\Network;

use Drupal\social_api\Plugin\NetworkInterface;

/**
 * Defines the itsme Auth interface.
 */
interface ItsmeAuthInterface extends NetworkInterface {

}
